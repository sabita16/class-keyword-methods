# the class keyword methods to accept coordinates as a pair of tuples
# and return the slope and distance of the line.


class Line:
    def __init__(self, coor1, coor2):
        self.coor1 = coor1
        self.coor2 = coor2
    def distance(self):
        x1, y1 = self.coor1
        x2, y2 = self.coor2
        d = ((x2-x1)**2+(y2-y1)**2)**0.5
        print(d)
    def slope (self):
        x1, y1 = self.coor1
        x2, y2 = self.coor2
        x = (y2-y1) / (x2-x1)
        print(x)

c1 = (3, 2)
c2 = (8, 10)

myline = Line(c1, c2)
myline.distance()
myline.slope()


#2
class Cylinder:
    def __init__(self, height = 1,radius= 1, pi = 3.14):
        self.height = height
        self.radius = radius
        self.pi = pi

    def volume(self):
        print(self.height*self.pi*self.radius**2)

    def surface_area(self):
        print(2*self.pi*self.radius**2 + (2*self.pi*self.radius*self.height))

c = Cylinder(2,3)
c.volume()
c.surface_area()



